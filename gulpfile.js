'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
const minifycss = require('gulp-minify-css');
const browserSync = require('browser-sync').create();

gulp.task('default', ['sass'], function () {
  browserSync.init({
      server: "./"
  });

  gulp.watch('./sass/**/*.scss', ['sass']).on('change', browserSync.reload);
  gulp.watch('./index.html').on('change', browserSync.reload);
})

gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});
